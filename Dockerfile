FROM python:3.8

RUN apt-get update
RUN apt-get install git
RUN mkdir /testify
WORKDIR  /testify

COPY ./src ./
COPY ./requirements.txt ./
RUN pip install -r requirements.txt

CMD ["python", "manage.py", "runserver", "0:8000" ]