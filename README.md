  **Программа Testify для проведения тестов**

   _ Установка программы на виртуальной машине под управлением Ubuntu:_
    
    1. Клонируем проект из Gitlab в папку своего пользователя:
    git clone https://gitlab.com/shipmatic/testify2.git
	2. Устанавливаем Докер на сервер или проверяем docker --version
	3. Устанавливаем docker-compose  на сервер
	
	sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	
	sudo chmod +x /usr/local/bin/docker-compose
	
	4. Запускаем docker-compose up из места расположения (каталога) проекта, где находятся dockerfile
	5. Если установлен системный nginx, то его надо остановить:

	sudo systemctl stop nginx
	sudo systemctl disable nginx

	6. Запускаем docker-compose в фоновом режиме:  

	sudo docker-compose up -d

    7. Инициализируем БД Postgress в контейнере Docker:

    Выполняем последовательно команды:
       #  Запускаем терминал из контейнера
    docker-compose exec postgresql bash  
       # Логинимся как суперпользователь Postgres
    su - postgres 
       # Запускаем консольную утилиту:
	psql

	   # и вводим команды по созданию БД:

    CREATE DATABASE testify;
	CREATE USER admin WITH PASSWORD 'admin';
	ALTER ROLE admin SET client_encoding TO 'utf8';
	ALTER ROLE admin SET default_transaction_isolation TO 'read committed';
	ALTER ROLE admin SET timezone TO 'UTC';
	GRANT ALL PRIVILEGES ON DATABASE testify TO admin;
	ALTER USER admin CREATEDB;
    
    8. Собираем static-files, запускаем команду из контейнера backend 

    docker-compose exec backend python manage.py collectstatic 

    Приложение установлено и готово к работе по адресу из панели управления AWS:

    Public IPv4 address

    9. После установки, необходимо ввести учетные данные Администратора для создания и редактирования тестов, для этого в контейнере backend необходимо выполнить команду:
    
    docker-compose exec backend bash
    python manage.py createsuperuser





	
