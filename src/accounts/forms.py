from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from accounts.models import User


class AccountCreateForm(UserCreationForm):
  
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'show_result', 'image']


class AccountUpdateForm(UserChangeForm):
    
    class Meta(UserChangeForm.Meta):
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'show_result', 'image']
