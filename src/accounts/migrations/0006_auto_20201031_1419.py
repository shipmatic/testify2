# Generated by Django 3.1.2 on 2020-10-31 12:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_auto_20201031_1412'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='show_result',
            field=models.BooleanField(default=False, verbose_name='Show results popup'),
        ),
    ]
