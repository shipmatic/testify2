# Generated by Django 3.1.2 on 2020-10-25 19:03

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('testify', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='testresult',
            name='current_order_number',
            field=models.PositiveSmallIntegerField(default=0, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(20)]),
            preserve_default=False,
        ),
    ]
