from django.contrib import admin

from testify.forms import AnswerInlineFormSet, QuestionForm, QuestionsInlineFormSet

from testify.models import Answer, Question, Test, TestResult, Topic

# Register your models here.


class AnswerInline(admin.TabularInline):
    model = Answer
    fields = ('text', 'is_correct')
    show_change_link = True
    formset = AnswerInlineFormSet
    extra = 0


class QuestionAdmin(admin.ModelAdmin):
    inlines = (AnswerInline,)
    form = QuestionForm

    
class QuestionsInline(admin.TabularInline):
    model = Question
    fields = ('text', 'order_number')
    show_change_link = True
    extra = 0
    ordering = ('order_number',)
    formset = QuestionsInlineFormSet
    form = QuestionForm


class TestAdmin(admin.ModelAdmin):
    inlines = (QuestionsInline,)


admin.site.register(Topic)
admin.site.register(Test, TestAdmin)  # расширяем тест ответами 
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer)
admin.site.register(TestResult)
