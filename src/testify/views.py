from django.shortcuts import redirect, render
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin

from django.urls.base import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views import View
from accounts.models import User

from testify.forms import AnswerFormSet
from testify.models import Question, Test, TestResult

from django.contrib import messages


# Create your views here.


class TestListView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('accounts:login')
    model = Test
    template_name = 'tests-list.html'
    context_object_name = 'tests'
    paginate_by = 5


class TestDetailView(LoginRequiredMixin, DetailView):
    model = Test
    template_name = 'details.html'
    context_object_name = 'test'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['continue_flag'] = TestResult.objects.filter(
            user=self.request.user,
            test=self.get_object(),
            state=TestResult.STATE.NEW,

        ).count()
        #  Записываем QuerrySet в словарь по ключу last_run, 
        context['last_run'] = TestResult.objects.filter(
            user=self.request.user,
            test=self.get_object(),
            state=TestResult.STATE.FINISHED,

        ).order_by('-write_date').first()

        context['best_result'] = TestResult.objects.filter(test=self.get_object()).extra(select={'points': 'num_correct_answers - num_incorrect_answers', 'duration': 'write_date - create_date'}, order_by=['-points', 'duration']).first()        
        context['results'] = TestResult.objects.filter(
            user=self.request.user,
            test=self.get_object(),
        ).order_by('-write_date')
        context['open_run'] = TestResult.objects.filter(
            user=self.request.user,
            test=self.get_object(),
            state=TestResult.STATE.NEW,
        ).first()

        return context


class TestRunnerView(LoginRequiredMixin, View):

    def get(self, request, id):
        current_user_tests = TestResult.objects.filter(
            user=request.user,
            test=Test.objects.get(id=id),
            state=TestResult.STATE.NEW
        )
        if current_user_tests.count() == 0:
            TestResult.objects.create(
                user=request.user,
                state=TestResult.STATE.NEW,
                test=Test.objects.get(id=id),
                num_correct_answers=0,
                num_incorrect_answers=0,
                current_order_number=1,
            )

        return redirect(reverse('tests:next', args=(id,)))


class QuestionView(LoginRequiredMixin, View):
    def get(self, request, id):
        # test_result = get_object_or_404(TestResult.objects.filter(
        #     user=request.user,
        #     test=Test.objects.get(id=id),
        #     state=TestResult.STATE.NEW
        # ))

        test_results = TestResult.objects.filter(
            user=request.user,
            state=TestResult.STATE.NEW,
            test=Test.objects.get(id=id),
        )
        if test_results.count() == 0:
            return redirect(reverse('tests:details', args=(id,)))

        test_result = test_results.first()

        order_number = test_result.current_order_number
        question = Question.objects.get(test__id=id, order_number=order_number)
        answers = question.answers.all()
        form_set = AnswerFormSet(queryset=answers)

        return render(
            request=request,
            template_name='question.html',
            context={
                'question': question,
                'form_set': form_set,
            }
        )

    def post(self, request, id):
        # test_result = get_object_or_404(TestResult.objects.filter(
        #     user=request.user,
        #     test=Test.objects.get(id=id),
        #     state=TestResult.STATE.NEW
        # ))

        test_results = TestResult.objects.filter(
            user=request.user,
            state=TestResult.STATE.NEW,
            test=Test.objects.get(id=id),
        )
        if test_results.count() == 0:
            return redirect(reverse('tests:details', args=(id,)))

        test_result = test_results.first()

        order_number = test_result.current_order_number
        question = Question.objects.get(test__id=id, order_number=order_number)
        answers = question.answers.all()
        form_set = AnswerFormSet(data=request.POST)
        show_result = test_result.user.show_result

        possible_choices = len(form_set.forms)
        selected_choices = [

            'is_selected' in form.changed_data
            for form in form_set.forms
        ]

        correct_choices = sum(
            answer.is_correct == choice
            for answer, choice in zip(answers, selected_choices)
        )

        point = int(correct_choices == possible_choices)

        test_result = TestResult.objects.get(
            user=request.user,
            test=question.test,
            state=TestResult.STATE.NEW
        )

        test_result.num_correct_answers += point
        test_result.num_incorrect_answers += (1 - point)
        test_result.current_order_number += 1
        test_result.save()
        if show_result is True: 
            if point == 1:
                messages.info(request, 'Well done, correct answer')
            else:
                messages.warning(request, 'Incorrect answer')

        if order_number == question.test.questions.count():
            test_result.state = TestResult.STATE.FINISHED
            test_result.save()

            User.objects.filter(
                username=request.user,
            ).update(rating=request.user.rating + test_result.points())

            return render(
                request=request,
                template_name='finish.html',
                context={
                    'test_result': test_result,
                    # 'time_spent': test_result.write_date - test_result.create_date,
                    'test_result_score': test_result.num_correct_answers / test_result.test.questions.count() * 100

                }
            )
        else:
            return redirect(reverse('tests:next', args=(id,)))
