from django import forms
from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet, ModelForm, modelformset_factory


from testify.models import Answer, Question


class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = '__all__'

    def clean(self):
        pass


class AnswerForm(ModelForm):
    is_selected = forms.BooleanField(required=False)
    # test = fields.CharField()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['text'].label = ""
        self.fields['is_selected'].label = ""

    class Meta:
        model = Answer
        fields = ['text', 'is_selected']
        widgets = {
            'text': forms.TextInput(attrs={'readonly': True})
        }


AnswerFormSet = modelformset_factory(
    model=Answer,
    form=AnswerForm,
    extra=0
)


class QuestionsInlineFormSet(BaseInlineFormSet):

    def clean(self):  # проверяем количество вопросов берем из models 
        if not (self.instance.QUESTION_MIN_LIMIT <= len(self.forms) <= self.instance.QUESTION_MAX_LIMIT):
            raise ValidationError('Quantity of questions is out of range ({}..{})'.format(
                self.instance.QUESTION_MIN_LIMIT, self.instance.QUESTION_MAX_LIMIT
            ))

        # counters = [question.order_number for question in self.instance.questions.all()]
        lst = [form.cleaned_data['order_number'] for form in self.forms]
        lst.sort()
        check = [i for i in range(1, len(lst) + 1)]
        if lst != check: 
            raise ValidationError('Order number must be within limits ')


class AnswerInlineFormSet(BaseInlineFormSet):

    def clean(self):  # проверяем количество ответов
        if not (self.instance.ANSWER_MIN_LIMIT <= len(self.forms) <= self.instance.ANSWER_MAX_LIMIT):
            raise ValidationError('Quantity of answers is out of range ({}..{})'.format(
                self.instance.ANSWER_MIN_LIMIT, self.instance.ANSWER_MAX_LIMIT
            ))

        num_correct_answers = sum([
            1
            for form in self.forms
            if form.cleaned_data['is_correct']
        ])

        if num_correct_answers == 0:
            raise ValidationError('At LEAST one answer must be correct!')

        if num_correct_answers == len(self.forms):
            raise ValidationError('Not allowed to select ALL answers!')

        

