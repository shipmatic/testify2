from django.forms.models import inlineformset_factory
from django.test import TestCase

from testify.forms import AnswerForm, QuestionsInlineFormSet, QuestionForm, AnswerInlineFormSet
from testify.models import Question, Test, Answer


class AdminQuestionsInlineFormSetTest(TestCase):
    # fixtures = [
    #     'tests/fixtures/dump.json',
    # ]

    def setUp(self):
        self.test = Test.objects.first()

        self.QuestionFormSet = inlineformset_factory(
            Test, Question, QuestionForm, formset=QuestionsInlineFormSet)

        self.data = {
            'questions-INITIAL_FORMS': 0,
            'questions-MAX_NUM_FORMS': 1000,
            'questions-TOTAL_FORMS': 3,
            'questions-0-text': '2+2',
            'questions-0-order_number': '1',
            'questions-1-text': '3*3',
            'questions-1-order_number': '2',
            'questions-2-text': 'sqrt(0)',
            'questions-2-order_number': '3',
        }

    def test_formset_is_valid_if_questions_number_is_in_range(self):
        formset = self.QuestionFormSet(self.data, instance=self.test)
        self.assertEqual(formset.is_valid(), True)

    def test_formset_is_invalid_if_questions_number_is_out_of_range_max(self):
        self.data['questions-TOTAL_FORMS'] = Test.QUESTION_MAX_LIMIT + 1
        formset = self.QuestionFormSet(self.data, instance=self.test)
        self.assertEqual(formset.is_valid(), False)

    def test_formset_is_invalid_if_questions_number_is_out_of_range_min(self):
        self.data['questions-TOTAL_FORMS'] = Test.QUESTION_MIN_LIMIT - 1
        formset = self.QuestionFormSet(self.data, instance=self.test)
        self.assertEqual(formset.is_valid(), False)

    def test_formset_is_invalid_if_order_number_is_incorrect(self):
        self.data['questions-2-order_number'] = '4'
        formset = self.QuestionFormSet(self.data, instance=self.test)
        self.assertEqual(formset.is_valid(), False)

        self.data['questions-2-order_number'] = self.data['questions-1-order_number']
        formset = self.QuestionFormSet(self.data, instance=self.test)
        self.assertEqual(formset.is_valid(), False)

        # self.data['questions-2-order_number'] = '-1'
        # formset = self.QuestionFormSet(self.data, instance=self.test)
        # self.assertEqual(formset.is_valid(), False)


class AdminAnswerInlineFormSetTest(TestCase):
    # fixtures = [
    #     'tests/fixtures/dump.json',
    # ]

    def setUp(self):
        # self.question = Question.objects.first()
        self.test = Test.objects.first()
        self.answer = Question.objects.first()

        self.AnswerFormSet = inlineformset_factory(
            Question, Answer, AnswerForm, formset=AnswerInlineFormSet) 

        self.data = {
            'answers-INITIAL_FORMS': 0,
            'answers-MAX_NUM_FORMS': 1000,
            'answers-TOTAL_FORMS': 3,
            'answers-0-text': '2+2',
            'answers-0-is_correct': 'on',
            'answers-1-text': '3*3',
            'answers-1-is_correct': 'on',
            'answers-2-text': 'sqrt(0)',
            'answers-2-is_correct': 'on',
        }

    def test_formset_is_valid_if_questions_number_is_in_range(self):
        formset = self.AnswerFormSet(self.data, instance=self.test)
        self.assertEqual(formset.is_valid(), True)
