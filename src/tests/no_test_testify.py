from django.test import TestCase
from django.test import Client
from django.urls import reverse

from testify.models import Test


class TestifyTest(TestCase):
    fixtures = [
        'tests/fixtures/dump.json',
    ]


def setUp(self):
    self.client = Client()
    self.client.login(username='admin', password='admin')


def test_success_flow(self):
    response = self.client.get(reverse('tests:start', kwargs={'id': self.TEST_ID}))
    self.assertRedirects(response, reverse('tests:next', kwargs={'id': self.TEST_ID}))

    test = Test.objects.get(id=self.TEST_ID)
    questions_count = test.questions.count()

    for step in range(1, questions_count + 1):
        next_url = reverse('tests:next', kwargs={'id': self.TEST_ID})
        response = self.client.get(next_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Next')
        
        count_answers = len(self.Test.objects.all().last().questions.get(index=step).answers.all())
        response = self.client.post(
            path=next_url,
            data={
                'form-TOTAL_FORMS': count_answers,
                'form-INITIAL_FORMS': count_answers,
                'form-MIN_NUM_FORMS': '0',
                'form-MAX_NUM_FORMS': '1000',
                # 'form-0-is_selected': 'on',
            }
        )
        if step < questions_count:
            self.assertRedirects(response, next_url)
        else:
            self.assertEqual(response.status_code, 200)

    self.assertContains(response, 'Congratulations!!!')



